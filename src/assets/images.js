import logo from './img/logo.png';
import product01 from './img/product_01.png';
import logoAvala from './img/logo_avalado.png';
import logoDanone from './img/logo_danone.png';
import berry01 from './img/berry_01.png';
import berry02 from './img/berry_02.png';
import berry03 from './img/berry_03.png';
import berry04 from './img/berry_04.png';
import logo100 from './img/logo_100.png';
import item01 from './img/item_01.png';
import item02 from './img/item_02.png';
import item03 from './img/item_03.png';


export const IMAGES = {
  'logo': logo,
  'logoAvala': logoAvala,
  'logo100': logo100,
  'logoDanone': logoDanone,
  'berry01': berry01,
  'berry02': berry02,
  'berry03': berry03,
  'berry04': berry04,
  'product01': product01,
  'item01': item01,
  'item02': item02,
  'item03': item03,
}
