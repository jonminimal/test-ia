export const THEME = {
  'id': '001',
  'name': 'Principal Theme',
  'colors': {
    'primary': '#501888',
    'secondary': '#BB9200',
    'acent': '#34D45C',
    'text': {
      'paragraph': '#495057',
      'title': '#212529'
    }
  },
  'fonts': {
    'acent': 'Open Sans',
    'body': 'Nunito'
  },
  'transition': {
    'basic': 'all 200ms ease'
  }
}
