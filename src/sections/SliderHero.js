import styled from 'styled-components';
import {IMAGES} from "../assets/images";
import {THEME} from '../theme/global';
import {useSpring, animated, config} from 'react-spring';

//Wrapper
const Wrapper = styled(animated.div)`
  position: relative;
  padding: 8rem 0 0 0;
  background-image: linear-gradient(to bottom, #fff2f2, #ffcbcb);
  transition: ${THEME.transition.basic};
  overflow: hidden;

  svg {
    position: absolute;
    bottom: 0;
  }
  
  @media (min-width: 800px) {
    padding: 8rem 0;
  }
`;

//Data container, image and description
const DataContainer = styled.div`
  padding: 0 2rem;

  @media (min-width: 800px) {
    display: grid;
    align-items: center;
    grid-template: auto / 2fr 2fr;
  }

  @media (min-width: 1000px) {
    padding: 1rem 4rem;
  }

  @media (min-width: 1200px) {
    padding: 1rem 8rem;
  }
`;

//Image Product
const ProductContainer = styled.div`
  width: 100%;
  height: 24rem;
  margin: auto auto 4rem auto;
  overflow: hidden;

  @media (min-width: 800px) {
    margin: auto auto;
    height: 32rem;
  }
`;

const ProductImage = styled.img`
  width: 100%;
  height: 100%;
  object-fit: contain;
  transform: rotate(15deg);
  overflow: hidden;
`;

const InfoWrapp = styled.div`
  height: fit-content;
  padding-bottom: 8rem;

  @media (min-width: 800px) {
    padding-bottom: 0;
  }
`;

const PillsContainer = styled.div`
  span {
    margin-right: 1rem;
  }
`;

const TextContainer = styled.div`

  h1 {
    font-size: 2rem;
    color: ${THEME.colors.primary};

    @media (min-width: 800px) {
      font-size: 2.25rem;
    }
  }

  p {
    margin-bottom: 2rem;
  }
`;

const LogosContainer = styled.div`
  margin-top: 2rem;
`;

const LogoSmall= styled.img`
  width: 4rem;
  height: 4rem;
  margin-right: 1rem;
`;


// Elements
const Button = styled.a`
  display: block;
  width: fit-content;
  padding: 1rem 2rem;
  border-radius: 1rem;
  font-weight: 600;
  color: white;
  background: ${THEME.colors.primary};
  cursor: pointer;
`;

const PillTab = styled.a`
  margin-right: 1rem;
  padding: .25rem 1rem;
  border: 1px solid #ffc2ce;
  border-radius: 1rem;
  font-size: .875rem;
  font-weight: ${props => props.active ? '700' : 'normal'};
  color: ${THEME.colors.text.title};
  background: ${props => props.active ? '#ffc2ce' : 'transparent'};
  cursor: pointer;
  transition: ${THEME.transition.basic};

  :hover {
    background: ${props => props.active ? '#ffc2ce' : 'rgba(255, 194, 206, 0.25)'};
  }
`;

//Pill tab component
export const Pill = ({label, ...props}) => {
  return (
    <PillTab active={props.active}>{label || 'Label'}</PillTab>
  )
};


// Component
const SliderHero = () => {

  const styles = useSpring({
    to: { opacity: 1 },
    from: { opacity: 0 },
    reset: true,
    delay: 100,
    config: config.molasses,
  })

  return (
    <Wrapper style={styles}>
      <svg height="100px" width="100%" viewBox="0 0 100 100" preserveAspectRatio="none" fill='white'>
        <path d="M0 0 L100 100 L0 100 Z"/>
      </svg>
      <DataContainer>
        <ProductContainer>
          <ProductImage src={IMAGES.product01}/>
        </ProductContainer>
        <InfoWrapp>
          <PillsContainer>
            <span>Sabor:</span>
            <Pill
              label={'Fresa'}
              active={true}
            />
            <Pill
              label={'Guayaba'}
            />
            <Pill
              label={'Toronja'}
            />
          </PillsContainer>
          <TextContainer>
            <h1>Vitalínea® Bebible Fresa 220g</h1>
            <p>¡La presentación ideal para llevar contigo! Vitalínea bebible es la opción si eres de las personas que
              siempre están activas y quieren probar algo sano, rico y práctico.</p>
            <Button>Comprar en Walmart</Button>
          </TextContainer>
          <LogosContainer>
            <LogoSmall src={IMAGES.logoAvala}/>
            <LogoSmall src={IMAGES.logo100}/>
          </LogosContainer>
        </InfoWrapp>
      </DataContainer>
    </Wrapper>
  );
};

export default SliderHero;
