import React from 'react';
import styled from 'styled-components';
import {Menu} from 'react-feather';
import {IMAGES} from "../assets/images";
import {THEME} from '../theme/global';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

const Wrapper = styled.div`
  position: fixed;
  display: flex;
  width: calc(100% - 2rem);
  top: 0;
  left: 0;
  justify-content: space-between;
  align-items: center;
  padding: .5rem 1rem;
  background: rgba(255, 255, 255, 0.75);
  transition: ${THEME.transition.basic};
  backdrop-filter: blur(10px);
  z-index: 10;

  @media (min-width: 1000px) {
    width: calc(100% - 12rem);
    padding: 1rem 6rem;
  }
`;

const LogoContainer = styled.div`
  width: 6rem;
  height: 4rem;
  background: url(${IMAGES.logo}) no-repeat center;
  background-size: contain;
  cursor: pointer;
`;

const MenuIcon = styled(Menu)`
  color: ${THEME.colors.secondary};
  cursor: pointer;

  @media (min-width: 800px) {
    display: none;
  }
`;

const NavContainer = styled.div`
  display: none;

  @media (min-width: 800px) {
    display: flex;
    align-items: center;
  }
`;

const NavLink = styled.span`
  margin-left: 2rem;
  font-weight: 600;
  line-height: 1;
  color: ${THEME.colors.text.paragraph};
  cursor: pointer;
  transition: ${THEME.transition.basic};

  :hover {
    color: ${THEME.colors.primary};
  }
`;

const SocialLink = styled(FontAwesomeIcon)`
  font-size: 1.75rem;
  color: ${THEME.colors.primary};
  margin-left: 2rem;
  cursor: pointer;
  transition: ${THEME.transition.basic};

  :hover {
  }
`;

const Header = () => {
  return (
    <Wrapper>
      <LogoContainer/>
      <div>
        <MenuIcon size={'2rem'}/>
        <NavContainer>
          <NavLink href="">Nuestros productos</NavLink>
          <NavLink href="">Disfruta cuidarte</NavLink>
          <NavLink href="">Blog</NavLink>
          <SocialLink icon={['fab', 'facebook']}/>
          <SocialLink icon={['fab', 'instagram']}/>
          <SocialLink icon={['fab', 'youtube']}/>
        </NavContainer>
      </div>
    </Wrapper>
  );
};

export default Header;
