import styled from 'styled-components';
import {THEME} from '../theme/global';
import {IMAGES} from '../assets/images';

const Wrapper = styled.div`
  position: relative;
  padding: 2rem 1rem;
  font-size: .75rem;
  color: ${THEME.colors.text.title};
  text-align: center;

  @media (min-width: 800px) {
    padding: 2rem 2rem 0 2rem;
  }
`;

const LogosContainer = styled.div`
  display: flex;
  margin-bottom: 2rem;
  justify-content: space-between;
  align-items: center;
`;

const Logo = styled.img`
  width: 4rem;
  height: auto;
`;

const TextsContainer = styled.div`
  @media (min-width: 800px) {
    position: absolute;
    top: 2.75rem;
    left: 7rem;
    text-align: left;
  }
`;

const LinksContainer = styled.div`

  span {
    margin: 0 .5rem;
  }

  span:last-of-type {
    display: none;

    @media (min-width: 800px) {
      display: inline;
    }
  }

  a:last-child {
    display: block;

    @media (min-width: 800px) {
      display: inline;
    }
  }

`;

const Link = styled.a`
  font-weight: 700;
  cursor: pointer;

  :hover {
    color: ${THEME.colors.primary};
  }
`;

const Footer = () => {
  return (
    <Wrapper>
      <LogosContainer>
        <Logo src={IMAGES.logoDanone}/>
        <Logo src={IMAGES.logo}/>
      </LogosContainer>
      <TextsContainer>
        <LinksContainer>
          <Link>Términos y condiciones</Link>
          <span>|</span>
          <Link>Políticas de privacidad</Link>
          <span>|</span>
          <Link>Aviso de privacidad</Link>
        </LinksContainer>
        <div>
          <p>Danone de México 2021©. Todos los derechos reservados 2021</p>
        </div>
      </TextsContainer>
    </Wrapper>
  );
};

export default Footer;
