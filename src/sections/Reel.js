import styled from "styled-components";
import {animated} from "react-spring";
import {THEME} from "../theme/global";
import {IMAGES} from "../assets/images";

const Wrapper = styled(animated.div)`
  position: relative;
  padding: 4rem 0;
  color: ${THEME.colors.primary};
  background-image: linear-gradient(to bottom, #fff2f2, #ffcbcb);
  transition: ${THEME.transition.basic};
  overflow: hidden;

  @media (min-width: 800px) {
    padding: 6rem 0;
  }

  h2 {
    text-align: center;

    @media (min-width: 800px) {
      font-size: 2rem;
    }
  }
`;

const ItemsContainer = styled.div`
  display: flex;
  overflow-x: scroll;
  -ms-overflow-style: none;
  scrollbar-width: none;
  -webkit-overflow-scrolling: touch;

  ::-webkit-scrollbar {
    display: none;
  }


  @media (min-width: 800px) {
    overflow: hidden;
    justify-content: center;
  }
`;

const WrapItem = styled.div`
  display: flex;
  margin: 0 1rem;
  flex-wrap: wrap;
  width: 100%;
  justify-content: center;
`;

const Image = styled.img`
  width: auto;
  height: 24rem;
`;

const Name = styled.span`
  display: block;
  width: 100%;
  font-size: 1.175rem;
  text-align: center;

  @media (min-width: 800px) {
    font-size: 1.5rem;
  }
`;

export const Item = ({name, image}) => {

  return (
    <div style={{display: 'inline'}}>
      <WrapItem>
        <Image src={image}/>
        <Name>{name}</Name>
      </WrapItem>
    </div>
  );
};

const Reel = () => {
  return (
    <Wrapper>
      <h2>La familia Vitalínea®</h2>
      <ItemsContainer>
        <Item
          image={IMAGES.item02}
          name='Vitalínea Sin Azúcar'
        />
        <Item
          image={IMAGES.item01}
          name='Vitalínea Griego'
        />
        <Item
          image={IMAGES.item03}
          name='Vitalínea Bebible'
        />
      </ItemsContainer>
    </Wrapper>
  );
};

export default Reel;
