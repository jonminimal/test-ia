import styled from "styled-components";
import {THEME} from "../theme/global";

const Wrapper = styled.div`
  margin: 4rem 0;
  padding: 0 2rem;

  h2 {
    color: ${THEME.colors.primary};

    @media (min-width: 800px) {
      font-size: 2rem;
    }
  }

  @media (min-width: 1000px) {
    padding: 0 8rem;
  }
  
  @media (min-width: 1400px) {
    padding: 0 16rem;
  }
`;

const DataContainer = styled.div`

  @media (min-width: 800px) {
    display: flex;
  }
`;

const TextContainer = styled.div`

  @media (min-width: 800px) {
    flex-shrink: 1.5;
    margin-right: 2rem;
  }

  @media (min-width: 1000px) {
    margin-right: 4rem;
  }
  
  div {
    margin-bottom: 2rem;

    p {
      margin-top: 0;
    }
  }
  
  div:last-child {
    margin-bottom: 3rem;
  }

  span {
    display: block;
    font-weight: 700;
  }
`;

const TableContainer = styled.table`
  border: none;
  font-size: .875rem;
  color: ${THEME.colors.primary};

  tr {
    background: #f1e6ff;
  }

  tr:nth-child(odd) {
    background: #f9f4ff;
  }

  td {
    padding: .5rem;
  }

  td:first-child {
    text-align: right;
  }

  td:last-child {
    font-weight: 600;
  }
`;


const Table = () => {
  return (
    <Wrapper>
      <h2>Información nutrimental</h2>
      <DataContainer>
        <TextContainer>
          <div>
            <span>Vitalínea® Bebible Guayaba 217gr</span>
            <span>Valor promedio por porción de 217gr</span>
            <span>Porciones por envase: 1</span>
          </div>
          <div>
            <span>Ingredientes:</span>
            <p>Leche descremada pasteurizada y/o reconstituida pasteurizada de vaca. 3.5% preparado de fruta guayaba
              (acesulfame K y sucralosa (27.2mg/100g)), crema, almidón modificado, maltodextrina y cultivos
              lácticos.</p>
          </div>
        </TextContainer>
        <TableContainer width={'100%'}>
          <tr>
            <td>Contenido energético kJ/kcal</td>
            <td>413,2/97,9</td>
          </tr>
          <tr>
            <td>Proteínas (g)</td>
            <td>5.3</td>
          </tr>
          <tr>
            <td>Grasas (lípidos) (g)</td>
            <td>2.1</td>
          </tr>
          <tr>
            <td>Grasas saturadas (g)</td>
            <td>1.4</td>
          </tr>
          <tr>
            <td>Carbohidratos (Hidratos de carbono) (g)</td>
            <td>12.4</td>
          </tr>
          <tr>
            <td>Azúcares (g)</td>
            <td>7.7</td>
          </tr>
          <tr>
            <td>Azúcares añadidos (g)</td>
            <td>0.1</td>
          </tr>
          <tr>
            <td>Fibra dietética (g)</td>
            <td>0.0</td>
          </tr>
          <tr>
            <td>Sodio (mg)</td>
            <td>92.7</td>
          </tr>
          <tr>
            <td>Calcio (mg)</td>
            <td>199.6</td>
          </tr>
          <tr>
            <td>%VNR*</td>
            <td>22</td>
          </tr>
        </TableContainer>
      </DataContainer>
    </Wrapper>
  );
};

export default Table;
