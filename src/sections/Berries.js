import styled from 'styled-components';
import {IMAGES} from "../assets/images";
import {useSpring, animated, config} from 'react-spring';
import React from "react";

const BerryTopLeft = styled(animated.img)`
  position: absolute;
  top: -2rem;
  left: -25%;
  
  @media (min-width: 800px) {
    top: -2%;
    left: 5%;
  }
`;

const BerryTopMiddle = styled(animated.img)`
  position: absolute;
  top: -8rem;
  right: -30%;

  @media (min-width: 800px) {
    right: 15%;
  }
`;

const BerryMiddleLeft = styled(animated.img)` 
  position: absolute;
  display: none;

  @media (min-width: 800px) {
    display: block;
    top: 30rem;
    left: -15%;
  }
`;

const BerryMiddleLeft2 = styled(animated.img)` 
  position: absolute;
  display: none;
  
  @media (min-width: 1400px) {
    display: block;
    top: 54rem;
    left: -3%;
  }
`;

const BerryMiddleRight = styled(animated.img)`
  position: absolute;
  display: none;

  @media (min-width: 800px) {
    display: block;
    top: 30rem;
    right: 0;
  }
`;

const BerryMiddleRight2 = styled(animated.img)`
  position: absolute;
  top: 60rem;
  right: -30%;
  
  @media (min-width: 800px) {
    top: 55rem;
    right: -15%;
  }
  
  @media (min-width: 1400px) {
    right: -5%;
  }
`;

const BerryBottomLeft = styled(animated.img)`
  position: absolute;
  display: none;

  @media (min-width: 800px) {
    display: block;
    top: 76rem;
    left: -10%;
  }
`;

const BerryBottomLeft2 = styled(animated.img)`
  position: absolute;
  top: 114em;
  left: -35%;

  @media (min-width: 800px) {
    display: block;
    top: 80em;
    left: 15%;
  }
`;

const BerryBottomRight = styled(animated.img)`
  position: absolute;
  display: none;
  top: 60rem;
  right: -30%;
  
  @media (min-width: 800px) {
    display: block;
    top: 80rem;
    right: -20%;
  }
  
  @media (min-width: 1400px) {
    right: -5%;
  }
`;

const Berries = () => {

  const stylesBerryTopLeft = useSpring({
    to: { opacity: 1, scale: .6, filter: 'blur(0px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.06))' },
    from: { opacity: 0, scale: 1, scaleX: '-1', filter: 'blur(10px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.03))' },
    delay: 500,
    config: config.molasses,
  })

  const stylesTopMiddle = useSpring({
    to: { opacity: 1, scale: 1, filter: 'blur(10px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.03))' },
    from: { opacity: 0, scale: .75, filter: 'blur(0px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.1))' },
    delay: 750,
    config: config.molasses,
  })

  const stylesMiddleLeft = (useSpring)({
    to: { opacity: 1, scale: 1.25, filter: 'blur(5px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.03))'},
    from: { opacity: 0, scale: 1, filter: 'blur(0px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.1))'},
    delay: 750,
    config: config.molasses,
  })

  const stylesMiddleLeft2 = (useSpring)({
    to: { opacity: 1, scale: .65, filter: 'blur(0px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.06))'},
    from: { opacity: 0, scale: 1.25, filter: 'blur(10px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.03))'},
    delay: 800,
    config: config.molasses,
  })

  const stylesMiddleRight = (useSpring)({
    to: { opacity: 1, scale: .6, filter: 'blur(0px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.06))' },
    from: { opacity: 0, scale: 1, scaleX: '-1', filter: 'blur(10px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.03))' },
    delay: 500,
    config: config.molasses,
  })

  const stylesMiddleRight2 = (useSpring)({
    to: { opacity: 1, scale: .75, filter: 'blur(5px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.03))' },
    from: { opacity: 0, scale: .5, filter: 'blur(0px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.1))' },
    delay: 800,
    config: config.molasses,
  })

  const stylesBottomLeft = (useSpring)({
    to: { opacity: 1, scale: .75, filter: 'blur(0px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.06))' },
    from: { opacity: 0, scale: 1.25, filter: 'blur(10px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.03))' },
    delay: 500,
    config: config.molasses,
  })

  const stylesBottomLeft2 = (useSpring)({
    to: { opacity: 1, scale: .65, filter: 'blur(0px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.06))' },
    from: { opacity: 0, scale: 1, filter: 'blur(10px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.03))' },
    delay: 500,
    config: config.molasses,
  })

  const stylesBottomRight = (useSpring)({
    to: { opacity: 1, scale: 1.25, filter: 'blur(6px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.03))' },
    from: { opacity: 0, scale: .5, filter: 'blur(0px) drop-shadow(0 6rem 1rem rgba(0, 0, 0, 0.1))' },
    delay: 800,
    config: config.molasses,
  })

  return (
    <>
      <BerryTopLeft src={IMAGES.berry02} style={stylesBerryTopLeft} />
      <BerryTopMiddle src={IMAGES.berry04} style={stylesTopMiddle} />
      <BerryMiddleLeft src={IMAGES.berry02} style={stylesMiddleLeft} />
      <BerryMiddleRight src={IMAGES.berry01} style={stylesMiddleRight} />
      <BerryMiddleLeft2 src={IMAGES.berry03} style={stylesMiddleLeft2} />
      <BerryMiddleRight2 src={IMAGES.berry01} style={stylesMiddleRight2} />
      <BerryBottomLeft src={IMAGES.berry02} style={stylesBottomLeft} />
      <BerryBottomLeft2 src={IMAGES.berry01} style={stylesBottomLeft2} />
      <BerryBottomRight src={IMAGES.berry04} style={stylesBottomRight} />
    </>
  );
};

export default Berries;
