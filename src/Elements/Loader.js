import {Player} from '@lottiefiles/react-lottie-player';
import spinner from '../../src/assets/animations/spinner.json';
import styled from "styled-components";

const Wrapper = styled.div`
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100vh;
  z-index: 10;
`;

const Loader = () => {
  return (
    <Wrapper>
      <Player
        autoplay
        loop
        src={spinner}
        style={{height: '100px', width: '100px'}}
      >
      </Player>
    </Wrapper>
  );
};

export default Loader;
