import Header from "./sections/Header";
import {library} from '@fortawesome/fontawesome-svg-core';
import {fab} from '@fortawesome/free-brands-svg-icons';
import styled from "styled-components";
import SliderHero from "./sections/SliderHero";
import Berries from "./sections/Berries";
import Table from "./sections/Table";
import Reel from "./sections/Reel";
import Footer from "./sections/Footer";

library.add(fab)

const BodyWrapper = styled.div`
  position: relative;
  width: 100%;
  overflow: hidden;
`;

function App() {

  return (
    <BodyWrapper>
      <Header/>
      <SliderHero/>
      <Table/>
      <Reel/>
      <Footer/>
      <Berries/>
    </BodyWrapper>
  );
}

export default App;
